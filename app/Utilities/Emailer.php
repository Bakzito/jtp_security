<?php

namespace App\Utilities;

use Illuminate\Support\Facades\Mail;

class Emailer
{

    private $to;
    private $from;
    private $cc;

    public function __construct($to, $from, $cc = '')
    {
        $this->to = $to;
        $this->from = $from;
        if ($cc) {
            $this->cc = $cc;
        }
    }

    public static function sendHtmlEmail($params, $template = "email.email")
    {
        Mail::send(["html" => $template], $params, function ($message) use ($params) {
            $message->to($params['email'])
                ->from('info@mytravelexpo.org.za')
                ->replyTo('info@travelexpo.org.za')
                ->subject($params['subject']);
        });

    }

    public function send($subject, $message)
    {
        try {
            if (!mail($this->to, $subject, $message, $this->getHeaders())) {
                throw new \Exception('Error sending email.');
            }

            return $this;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    private function getHeaders()
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <' . $this->from . '>' . "\r\n";
        $headers .= 'Cc:' . $this->cc . "\r\n";

        return $headers;
    }

}
