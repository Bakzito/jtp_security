<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;
Use App\Utilities\Emailer;


class ContactUs extends Controller
{
    public function sendMail(Request $request)
    {
        try {
            $data = [
                'name'=>$request['name'],
                'email'=>$request['email'],
                'subject'=>$request['subject'],
                'message_body'=>$request['message']
            ];

            Emailer::sendHtmlEmail($data);
            redirect('/');

        } catch (QueryException $e) {
        }
    }

}
