 <!-- ======= Footer ======= -->
 <footer id="footer">
     <div class="container">
         <h3>JTP SECURITY</h3>
         {{-- <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi
             fuga maxime saepe commodi placeat.</p> --}}
         <div class="social-links">
             <a class="twitter"><i class="bx bxl-twitter"></i></a>
             <a class="facebook"><i class="bx bxl-facebook"></i></a>
             <a class="instagram"><i class="bx bxl-instagram"></i></a>
             <a class="google-plus"><i class="bx bxl-skype"></i></a>
             <a class="linkedin"><i class="bx bxl-linkedin"></i></a>
         </div>
         <hr>
         <div class="row">
             <div class="col-lg-4">
                 <ul class="objectives pull-left text-left">
                     <h4>Contact</h4>
                     <li><i class="fa fa-phone"></i>&nbsp;+27 11 044 9484</li>
                     <li><i class="fa fa-mobile fa-lg"></i>&nbsp;&nbsp;+27 66 597 5376/ +27 68 551 5197 </li>
                     <li><i class="fa fa-envelope"></i>&nbsp;Admin@jtpsecurity.co.za</li>
                     <li><i class="fa fa-map-marker fa-lg"></i>&nbsp;&nbsp;16506 Mount fletcher Dr |Krugersdorp, 1754
                     </li>
                 </ul>
             </div>
             <div class="col-lg-4">
                 <ul class="objectives pull-left text-left">
                     <h4>Services</h4>
                     <li><i class=""></i> Supply of security personnel (Guards)</li>
                     <li><i class=""></i> Access controller</li>
                     <li><i class=""></i> Site patrol</li>
                     <li><i class=""></i> Vip protection</li>
                     <li><i class=""></i> Courts Escorts</li>
                     <li><i class=""></i> Event Security</li>
                 </ul>
             </div>
             <div class="col-lg-4">
                 <ul class="objectives pull-left text-left">
                     <h4>Links</h4>
                     <li class="active" class="js-anchor-link"><a href="/">Home</a></li>
                     <li><a href="#about" class="js-anchor-link">About</a></li>
                     <li><a href="#services" class="js-anchor-link">Services</a></li>
                     <li><a href="#contact" class="js-anchor-link">Contact</a></li>
                 </ul>
             </div>
             <hr>
         </div>

         <div class="copyright">
             &copy; Copyright <strong><span class="footer-jtp-name">JTP SECURITY</span></strong>. All Rights Reserved
         </div>
         <div class="credits">
             <!-- All the links in the footer should remain intact. -->
             <!-- You can delete the links only if you purchased the pro version. -->
             <!-- Licensing information: https://bootstrapmade.com/license/ -->
             <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/green-free-one-page-bootstrap-template/ -->
             {{-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
             --}}
         </div>
     </div>
 </footer><!-- End Footer -->
