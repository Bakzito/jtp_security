<!-- ======= Top Bar ======= -->
<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <i class="icofont-envelope"></i> <a href="mailto:contact@example.com">info@jtpsecurity.co.za</a>
            <i class="icofont-phone"></i> +27 66 597 5376
        </div>
        <div class="social-links">
            <a class="twitter"><i class="icofont-twitter"></i></a>
            <a class="facebook"><i class="icofont-facebook"></i></a>
            <a class="instagram"><i class="icofont-instagram"></i></a>
            {{-- <a class="skype"><i class="icofont-skype"></i></a>
            --}}
            <a class="linkedin"><i class="icofont-linkedin"></i></i></a>
        </div>
    </div>
</div>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        {{-- <h1 class="logo mr-auto"><a href="index.html">Green</a></h1>
        --}}

        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="index.html" class="logo mr-auto"><img src="theme/assets/img/logo.jpeg" alt="" class="img-fluid"></a>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="/">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#contact">Contact</a></li>

            </ul>
        </nav><!-- .nav-menu -->

        {{-- <a href="#about" class="get-started-btn scrollto">Get Started</a>
        --}}

    </div>
</header><!-- End Header -->
