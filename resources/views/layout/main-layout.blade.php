<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>JTP Security</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="theme/assets/img/jtp-favicon.png" rel="icon">
    <link href="theme/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="{{ asset('theme/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/text-reveal/text-reveal.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/preloader/preloader.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/vendor/accordion/accordion.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/particles/particles.css') }}" rel="stylesheet">


    <!-- Template Main CSS File -->
    <link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Green - v2.3.0
  * Template URL: https://bootstrapmade.com/green-free-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    <div class="loader">
        <div class="bounce"></div>
        <div class="bounce bouncer-1"></div>
        <div class="bounce bouncer-2"></div>
        {{-- <div class="dot-text"></div> --}}
    </div>

    <main id="main">

        @include('layout.header')
        @yield('content')
        @include('layout.footer')

    </main><!-- End #main -->
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('theme/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/venobox/venobox.min.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/text-reveal/text-reveal.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/preloader/preloader.js') }}"></script>
    <script src="{{ asset('theme/assets/vendor/scroll-up/scroll-up.js') }}"></script>
    <script src="{{ asset('theme/particles/particles.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('theme/assets/js/main.js') }}"></script>

</body>

</html>
